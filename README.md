# user2019-crrri

<!-- badges: start -->
<!-- badges: end -->

This contains the presentation for lightning talk at user2019 about
[crrri](https://github.com/RLesur/crrri/issues)

# Licence

https://creativecommons.org/licenses/by-sa/4.0/
